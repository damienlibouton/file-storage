package entity

import (
	"gopkg.in/mgo.v2/bson"
)

//ID type of entity objects
type ID bson.ObjectId

//NewID is the constructor of an ID
func NewID() ID {
	return StringToID(bson.NewObjectId().Hex())
}

//GetBSON implements bson.Getter
func (id ID) GetBSON() (interface{}, error) {
	if id == "" {
		return "", nil
	}
	return bson.ObjectId(id), nil
}

//SetBSON implements bson.Setter
func (id *ID) SetBSON(raw bson.Raw) error {
	decoded := new(string)
	bsonErr := raw.Unmarshal(decoded)
	if bsonErr == nil {
		*id = ID(bson.ObjectId(*decoded))
		return nil
	}
	return bsonErr
}

//MarshallJSON will convert marshall ID to Json
func (id ID) MarshallJSON() ([]byte, error) {
	return bson.ObjectId(id).MarshalJSON()
}

// UnmarshalJSON will convert a string to an ID
func (id *ID) UnmarshalJSON(data []byte) error {
	s := string(data)
	s = s[1 : len(s)-1]
	if bson.IsObjectIdHex(s) {
		*id = ID(bson.ObjectIdHex(s))
	}

	return nil
}

//String convert an ID in a string
func (id ID) String() string {
	return bson.ObjectId(id).Hex()
}

//StringToID convert a string to an ID
func StringToID(s string) ID {
	return ID(bson.ObjectIdHex(s))
}

//IsValidID check if is a valid ID
func IsValidID(s string) bool {
	return bson.IsObjectIdHex(s)
}

