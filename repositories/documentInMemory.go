package repositories

import (
	"errors"
	ar "gitlab.com/dlibout/file-storage/document"
	"gitlab.com/dlibout/file-storage/entity"
)

//DocumentInMemoryRepo is an object that implements the repository interface
//of the aggregate root (ar) Document.
type DocumentInMemoryRepo struct {
	dataMap map[string]*ar.Document
}

//NewDocumentInMemoryRepo is the contructor of the DocumentInMemoryRepo object
func NewDocumentInMemoryRepo() *DocumentInMemoryRepo {
	emptyMap := map[string]*ar.Document{}
	return &DocumentInMemoryRepo{
		dataMap: emptyMap}
}

//Find a document in the in-memory data source
func (r *DocumentInMemoryRepo) Find(id entity.ID) (*ar.Document, error) {
	if r.dataMap[id.String()] == nil {
		return nil, errors.New("Document to find not found with id : " + id.String())
	}
	return r.dataMap[id.String()], nil
}

//Store a document in the in-memory data source
func (r *DocumentInMemoryRepo) Store(doc *ar.Document) (entity.ID, error) {
	r.dataMap[doc.ID.String()] = doc
	return doc.ID, nil
}

//Delete a document in the in-memory data source
func (r *DocumentInMemoryRepo) Delete(id entity.ID) error {
	if r.dataMap[id.String()] == nil {
		return errors.New("Document to delete not found with id : " + id.String())
	}
	r.dataMap[id.String()] = nil
	return nil
}
