package services

import (
	ar "gitlab.com/dlibout/file-storage/document"
	"gitlab.com/dlibout/file-storage/entity"
	"time"
)

//DocumentService is an object exposing business functionalities
//of the aggregate root Document
type DocumentService struct {
	docRepo ar.Repository
	//To improve : add an interface container to inject a file storer system
}

//NewDocumentService is the constructor
func NewDocumentService(r ar.Repository) *DocumentService {
	return &DocumentService{
		docRepo: r}
}

//StoreDocument stores a document and return the id
//To improve : check if the document already exists before storing
func (svc *DocumentService) StoreDocument(name string, format string) (string, error) {
	doc := ar.NewDocument(entity.NewID(), name, format, time.Now())
	id, err := svc.docRepo.Store(doc)
	if err != nil {
		return "", err
	}
	return id.String(), nil
}

//FindDocument finds a document ar following an id
func (svc *DocumentService) FindDocument(id string) (*ar.Document, error) {
	return svc.docRepo.Find(entity.StringToID(id))
}
