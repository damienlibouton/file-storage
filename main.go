package main

import (
	"fmt"
	"reflect"
)

type itinerary struct {
	legs []leg
}

type leg struct {
	city string
}

func main() {
	fmt.Println("Implement the web server here")
	it1 := itinerary{legs: make([]leg, 3)}
	it1.legs[0].city = "Bxl"

	it2 := new(itinerary)
	if reflect.DeepEqual(it1, it2) {
		fmt.Println("Itineraries are equals")
	}

}
