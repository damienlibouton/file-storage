package document

import (
	"gitlab.com/dlibout/file-storage/entity"
	"time"
)

//Document is an aggregate root.
//It represents an archived document owned by any application by SMart.
type Document struct {
	entity.ID
	name     string
	format   string
	creation time.Time
}

//NewDocument is the constructor of the Document aggregate root
func NewDocument(id entity.ID, name string, format string, creation time.Time) *Document {
	return &Document{
		id,
		name,
		format,
		creation}
}

//UpdateName is a method of Document to update the name of a document
func (d *Document) UpdateName(name string) {
	d.name = name
}

//*****************************************************************************
//*****************************************************************************
//***********************INTERFACE DECLARATIONS********************************
//*****************************************************************************
//*****************************************************************************

//Repository is an interface. Must be implemented with any data source
type Repository interface {
	Find(id entity.ID) (*Document, error)
	Store(d *Document) (entity.ID, error)
	Delete(id entity.ID) error
}
